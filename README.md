# Ansible role - Softflowd daemon

This role serves for automatic installation of softflowd daemon, flow-based network traffic analyser.

## Requirements

This role requires root access, so you either need to specify `become` directive as a global or while invoking the role.

```yml
become: yes
```

## Role paramaters

* `softflowd_host` (mandatory) - The host where accounting datagrams are to be sent to. The host may be specified using a hostname or using a numeric IPv4 or IPv6 address. Numeric IPv6 addresses should be encosed in	square brackets	to avoid ambiguity between the address and the port.
* `softflowd_port` (mandatory) - The port at the host where accounting datagrams are to be sent to. The	destination port may be	a portname listed in [services(5)](https://www.freebsd.org/cgi/man.cgi?query=services) or a numeric port.
* `softflowd_interface` - Specify a network interface on which to listen for traffic or `any` to listen on every interface. (default `any`)
* `softflowd_max_flows` - Specify the maximum number of flows to concurrently track. (default `8192`)
* `softflowd_timeout` - List of strings of format `timeout_name=time`. More at [softflowd(8)](https://www.freebsd.org/cgi/man.cgi?query=softflowd).
* `softflowd_hoplimit` - Set the IPv4 TTL or the IPv6 hop limit to hoplimit otherwise default system TTL.
* `softflowd_track_level` - Specify which flow elements softflowd should be used to define a flow. track_level may be one of:
    * `full` - track everything in the flow (the default).
    * `proto` - track source and destination addresses and protocol.
    * `ip` - only track source and destination addresses.

For more information read documentation for Softflowd at [softflowd(8)](https://www.freebsd.org/cgi/man.cgi?query=softflowd).

## Example

Example of simple softflowd installation:

```yml
roles:
    - role: softflowd
      softflowd_host: 172.16.1.1
      softflowd_port: 6677
      softflowd_timeout:
          - tcp=300
      become: yes
```